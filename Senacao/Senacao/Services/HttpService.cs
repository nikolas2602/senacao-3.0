﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Senacao.Services
{
    class HttpService
    {
        static readonly HttpClient Client = new HttpClient();

        public static async Task<HttpResponseMessage>
            GetRequest(string url)
        {
            HttpResponseMessage response = await Client.GetAsync(url);
            return response;
        }

        public static async Task<HttpResponseMessage>
            PostRequest(string url, object obj)
        {
            //serializar objeto que sera enviado por parametro
            string json = JsonConvert.SerializeObject(obj);

            StringContent strContent = new
                StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await
                Client.PostAsync(url, strContent);
            return response;
        }

        public static JObject GetErrorDataFromHttpResponseMessage(HttpResponseMessage response)
        {
            var data = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            return data;
        }
            
    }
}
